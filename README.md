![cat](images/gato1.jpg)
# Nebulas explicado para gatos
## Una guía sencilla para entender qué es Nebulas, qué son los blockchains y qué son las criptodivisas

Este documento, dividido en tres capítulos, abarca de forma sencilla e intuitiva los conceptos básicos del mundo del _blockchain_, empezando por, justamente, la explicación de qué es un _blockchain_, para luego seguir por las _criptodivisas_, los _contratos inteligentes_ y otras tecnologías similares. En los dos últimos capítulos veremos qué es Nebulas, por qué es tan importante para el mundo del _blockchain_, y qué tecnologías está implementando. El último capítulo estará dedicado a servir como una guía para la instalación, configuración y uso de Nebulas. Puedes descargarlo en formato PDF desde aquí, si lo prefieres.

¿Empezamos? ¡Miau!

## Capítulo 1: introducción al mundo del _blockchain_

### Cadenas de bloques, criptodivisas, contratos inteligentes, aplicaciones descentralizadas...

Antes de siquiera intentar comprender qué es Nebulas, necesitamos partir de lo más básico, que es saber qué es un _blockchain_, para qué sirve, a qué se parece, y cómo decirlo en español.

#### Cadenas de bloques

La traducción al español de la palabra _blockchain_ es **cadena de bloques**.

En el lenguaje de los programadores, una _cadena_ es una serie de símbolos dispuestos uno al lado del otro, que pueden ser _cadenas de texto_ (por ejemplo: «`miau miau miau miau`») o bien cadenas binarias (que no se pueden representar directamente, ya que su representación directa contiene caracteres no imprimibles, aunque se pueden _transliterar_ en notación binaria, que es el lenguaje que entienden las computadoras: «`11101111100001001010001000100110`»).

Un bloque, a su vez, es un paquete de datos que almacena los registros de las transacciones realizadas en la red del _blockchain_ en el que se está trabajando. Por ejemplo, si envías 1 bitcoin a un amigo, la transacción quedará registrada en uno de los bloques del _blockchain_ de Bitcoin.

Podemos decir que un _blockchain_ (o una cadena de bloques) es esencialmente un _libro contable digital_, en donde cada transacción se convierte en un asiento contable que, una vez inscripto, ya no se puede modificar. Estos asientos se guardan en grupos (bloques), y los bloques a su vez se guardan en un libro (el blockchain).

Ahora bien, La idea del _blockchain_ es la de mantener un _libro contable digital_ que pueda ser _distribuido_ en distintas computadoras alrededor del mundo. Si sólo se necesitara una copia de ese libro, en una sola computadora, se utilizaría una solución clásica (como un servidor SQL); como la idea es distribuir copias idénticas de ese _libro contable_ entre varias computadoras —que sincronizan la información en base a un algoritmo de consenso—, es posible tener tres tipos de _blockchain_: privados, permisionados, y públicos.

##### Blockchains privados
Cuando es necesario mantener la privacidad de la información, se utiliza una cadena de bloques privada. Para acceder a la información almacenada es necesario autenticarse mediante credenciales.

Algunos bancos están utilizando este tipo de _blockchain_ para mantener una copia de la información financiera (balances, cuentas de los clientes, etcétera) repartida alrededor del mundo, sin los riesgos que conlleva tener un servidor centralizado.

Es importante entender que si bien el blockchain está distribuido en distintas computadoras alrededor del mundo, todas ellas pertenecen a la misma entidad.

##### Blockchains permisionados

El siguiente grado de exposición es el de los _blockchains permisionados_. Estos requieren roles, o _permisos_, para leer determinados registros. Así, por ejemplo, un _blockchain permisionado_ podría exponer unos pocos registros públicos y reservar los demás para usuarios con permisos apropiados.

Este tipo de blockchain es útil en el campo de la industria, en donde se requiere un registro multipartito entre fabricantes, proveedores, clientes y entidades de control, donde cada actor necesita tener acceso a determinados campos; los clientes seguramente necesitarán saber qué fecha de elaboración y qué número de lote tiene una pieza dada, pero no necesitan tener acceso a otra información, como el costo de cada componente (aunque esto sí estará disponible para el fabricante, que necesita esa información para elaborar reportes financieros).

En este caso es posible que existan copias del blockchain en computadoras ajenas a la entidad que maneja los datos (ya sea porque la información pertenece a distintos organismos, o porque hay un interés en delegar su mantenimiento al público en general a cambio de recompensas de minado, algo que veremos enseguida).

##### Blockchains públicos

Por último, los _blockchains públicos_ son _libros abiertos_ a cuyos datos cualquier persona puede acceder sin restricciones; la escritura se hace posible mediante un algoritmo de consenso que varía según el _blockchain_.

Los blockchains públicos están, en la inmensa mayoría de los casos, distribuidos en computadoras alrededor del mundo y en manos de cualquier persona; la información contenida en el blockchain se mantiene segura gracias a la implementación de un complejo algoritmo de consenso, que hace imposible la alteración de los registros en todas las computadoras al mismo tiempo: si se altera uno o más registros en una computadora, o en un grupo pequeño de ellas, sus blockchains se invalidan, ya que por consenso se considera inválida su información.

#### Criptodivisas, tokens, casas de cambio y minería

##### Criptodivisas

Una _criptodivisa_ es una moneda virtual que hace uso de un _registro descentralizado_ para almacenar la información de las transacciones a modo de _libro contable_. No todos los registros descentralizados tienen una criptodivisa asociada, y no todas las criptodivisas hacen uso de un _blockchain_ para almacenar sus transacciones; es posible tener _criptodivisas_ cuya base de datos están centralizadas (como en el caso de Google Wallet) o que se almacenan de forma distribuida utilizando otro tipo de tecnología, como el caso de los _Direct Acyclic Graphs_, de los cuales no hablaremos aquí.

La gran mayoría de las criptodivisas existentes son descentralizadas (esto significa que no existe ninguna autoridad nacional o supranacional que controle su flujo o que ejerza soberanía sobre ella), controladas por una comunidad abierta y libre (mediante la aplicación de complejos algoritmos de consenso), almacenadas en un blockchain, y por lo general con un valor fijado únicamente por las leyes de la oferta y la demanda.

##### Tokens

Los tokens son un tipo especial de criptodivisa, que podríamos explicar mediante analogía con las viejas _fichas_ o _cospeles_: así como una moneda sirve para comprar cualquier cosa en el mundo real, las _fichas_ o _cospeles_ (en inglés _tokens_) sirven para utilizar un servicio o una máquina en particular. Por ejemplo, un cospel telefónico servía para hacer uso de los viejos teléfonos públicos, aunque también existen todavía cospeles para hacer uso de lavarropas, parquímetros, o para abordar trenes subterráneos, tranvías, buses, etc.

En el mundo de los blockchains, los tokens no sólo se pueden utilizar para operar un servicio en particular, sino que también se pueden usar como cualquier otra criptodivisa, ya que se pueden vender y comprar en las decenas de casas de cambio de criptodivisa existentes.

##### Casas de cambio de criptodivisas

Estos servicios existen para poder cambiar distintos tipos de criptodivisas entre sí; por ejemplo, puedes vender tus Bitcoins y recibir Ether a cambio, o vender Ether y recibir NAS (la criptodivisa de Nebulas). Algunas casas de cambio permiten comprar criptodivisa con medios tradicionales de pago, como tarjetas de crédito, e incluso las hay que permiten vender criptodivisa y recibir dinero en una cuenta bancaria.

Muchas personas utilizan estas casas de cambio para hacer negocios beneficiándose de la volatilidad en el precio de cada criptodivisa. Existen varias formas de lograrlo: por arbitraje, por _scalping_, etcétera. Como el propósito de esta guía es el de servir de introducción al mundo del blockchain, no tocaremos este tema, que de por sí merece todo un libro aparte.

##### Minería

Para mantener a resguardo la información contenida en el _blockchain_, es necesario pergeñar algún sistema que impida la modificación de los datos contenidos en ellos. Para eso se diseñó un sistema criptográfico que protege la información del blockchain de modo tal que es trivial su lectura, pero virtualmente imposible su modificación.

Son varios los algoritmos creados para ello, pero todos tienen un punto en común, que es el _minero_: una computadora (o un grupo de ellas) encargadas de obtener la solución a un problema matemático de gran complejidad, que le demuestre al resto de los nodos de la red que efectivamente se ha realizado un _trabajo intensivo_ para lograr la encriptación de los datos. A cambio de ese esfuerzo computacional, al minero (o al grupo de mineros) se les entrega determinada recompensa en la criptodivisa asociada al blockchain, más una pequeña suma derivada de las comisiones que la red cobra por cada transacción. Este algoritmo se llama _Proof of Work_ y es, básicamente, el que utiliza Bitcoin para proteger su red.

Existen otros sistemas para proteger la información de un blockchain, como _Proof of Stake_, en el que no hay mineros sino más bien nodos que guardan una suma considerable de la criptodivisa asociada al blockchain, o cuya antigüedad en la red, sumada a la _fortuna_ que almacena, hacen que un ataque desde ese nodo sea económicamente inviable (ya que, de lograrlo, perdería todo o gran parte del valor intrínseco que almacena).

#### Contratos inteligentes

Ethereum introdujo el concepto de contrato inteligente: un programa informático que ejecuta una serie de instrucciones y que devuelve un valor (o un conjunto de valores) de acuerdo a determinadas condiciones pre-establecidas. Por ejemplo, un contrato inteligente podría servir para enviar, el primer día de cada mes, un porcentaje de las criptodivisas almacenadas en una dirección Ethereum a una lista de direcciones almacenada en el blockchain.

La importancia de los contratos inteligentes es crítica, ya que proveyó de interactividad al blockchain, haciendo que los mismos puedan tomar decisiones basándose en eventos, estados o datos.

#### Aplicaciones descentralizadas

Una aplicación descentralizada no es más que una pieza de software que agrupa distintos contratos inteligentes para realizar tareas más complejas y de forma descentralizada.

Un ejemplo podría ser el de un sistema de _escrow_ (fideicomiso) que permite realizar transacciones de riesgo sobre la red, como ser contrataciones, compraventa de bienes suntuorios, etcétera. La aplicación impone condiciones para el desembolso del dinero del comprador y para la entrega del producto o servicio; en caso de un conflicto que no puede ser resuelto por el sistema, se hace uso de _árbitros_ que reciben una recompensa (un porcentaje del monto total en fideicomiso) por solucionar el caso.

Por supuesto que lo descripto es sólo uno de los muchos ejemplos de uso.

## Capítulo 2: ¡Y llegó Nebulas!

Así como Ethereum introdujo el concepto de _blockchain interactivo_, capaz de correr código y hasta aplicaciones, Nebulas introdujo dos conceptos novedosos: el de la _valuación_ de los datos almacenados en los distintos blockchains, la posibilidad de realizar búsquedas dentro de ellos (para encontrar, por ejemplo, la aplicación descentralizada más apropiada para una tarea dada) y, en paralelo, el primer caso de incentivos automatizados dentro del blockchain y la posibilidad de tener un blockchain auto-evolutivo, capaz de actualizarse a sí mismo sin la necesidad de recurrir a _bifurcaciones_ (ya veremos este concepto un poco más adelante).

### ¿Pero qué es Nebulas?

[Es un proyecto que vio la luz en junio de 2017](https://es.wikipedia.org/wiki/Nebulas), cuya premisa es la de ofrecer una unidad de valuación de blockchains mediante el algoritmo **Nebulas Rank**, que mide el valor de cualquier blockchain público tomando en cuenta para ello la interacción entre sus usuarios, y la propagación y liquidez de sus activos digitales. También provee incentivos automáticos para los desarrolladores, mediante su sistema **Developer Incentive Protocol**, y que está construyendo las bases para su autoevolución mediante **Nebulas Force**. En paralelo a ello, está trabajando para poner en marcha un sistema de auto-gobernanza llamado Go Nebulas.

Pero veamos primero un poco la historia de este proyecto.

#### Fundación

Ocurrió en junio de 2017, cuando se constituyó el equipo de Nebulas. Ese mismo mes se lanzó el [sitio web](https://nebulas.io) y se publicó el [whitepaper no-técnico](https://nebulas.io/docs/NebulasWhitepaper.pdf).

#### Pilares de la Creación

En marzo de 2018, luego de la publicación del [whitepaper técnico](https://nebulas.io/docs/NebulasTechnicalWhitepaper.pdf) y del desarrollo y las pruebas de la _testnet_, se llegó al momento de la inauguración oficial de la _mainnet_ y, con ello, el lanzamiento de Nebulas Eagle, la primera versión de Nebulas, y el inicio de la vida de su blockchain en producción.

Nebulas utiliza nombres de nebulosas para designar sus versiones. La primera de ellas toma su nombre de la [Nebulosa del Águila](https://es.wikipedia.org/wiki/Nebulosa_del_%C3%81guila).

#### Nebulas Nova

[El lunes 15 de abril de 2019 se lanzó la segunda versión de Nebulas](https://medium.com/nebulasio/the-future-of-collaboration-nebulas-nova-official-launch-on-the-nebulas-mainnet-2863dec8a6e6), llamada NOVA. Esta nueva versión implementa los algoritmos [NBRE (Nebulas Blockchain Runtime Environment)](https://www.youtube.com/watch?v=BR9SNsWvnEc), [NR (Nebulas Rank)](https://www.youtube.com/watch?v=q0Y9yzzGvMM) y [DIP (Developer Incentive Protocol)](https://www.youtube.com/watch?v=sTSjRfa2rE4). También avanza hacia la realización de la metanet autónoma (cuya fecha estimada de lanzamiento será en diciembre de 2019).

### Características de Nebulas

#### Un ecosistema amigable para los _gatos_

Con el lanzamiento de [Go Nebulas](https://go.nebulas.io/) el 25 de marzo de 2019, se cumplió uno de los puntos más ambiciosos del proyecto Nebulas: contar con una plataforma de colaboración comunitaria en el que los miembros de la comunidad pueden proponer diversos proyectos y, mediante una votación, resultar elegidos para llevarlos adelante; muchos de esos proyectos son pagos, y la recompensa es el NAS, la criptodivisa nativa de Nebulas. De hecho, esta guía forma parte de una serie de trabajos propuestos en la plataforma.

Cualquier persona puede inscribirse allí, proponer un trabajo (relacionado a Nebulas), presupuestarlo y, de ser aprobado por la comunidad, una vez entregado, recibir los NAS presupuestados.

La mayoría de las propuestas de la plataforma son de desarrollo de contratos inteligentes y aplicaciones descentralizadas, aunque es posible proponer otro tipo de trabajos, como promoción en redes sociales, traducciones, creación de tutoriales, diseño, y más. ¡Sólo hay que ser valiente y proponer algo en lo que seas muy bueno!

#### NAS, la criptodivisa de Nebulas

Así como Bitcoin y Ethereum ofrecen las criptodivisas BTC y ETH respectivamente, Nebulas ofrece la suya, llamada NAS. En un primer momento fue simplemente un _token_ dentro de la red Ethereum, pero desde marzo de 2018, con el lanzamiento de Nebulas Eagle, NAS se convirtió en una criptodivisa completamente independiente, asociada al blockchain de Nebulas, [y con valor propio](https://coinmarketcap.com/currencies/nebulas-token/).

Como ocurre con otras criptodivisas, NAS sirve como método de pago dentro del proyecto (por ejemplo, para pagar por los desarrollos en el programa de incentivos), y se puede intercambiar por otras criptodivisas, o por dinero, [en las principales casas de cambio](https://www.binance.com/en/trade/NAS_BTC).

![Total de NAS en existencia](images/6.png)

Existe un total de 100 000 000 de NAS en existencia, de los cuales el 75% está disponible para los usuarios en general, 20% está reservado para el equipo de Nebulas y el 5% restante se mantiene como fondos de reserva. El desglose del total se puede consultar en la imagen de más arriba.

#### Nebulas Rank: un sistema de valuación de los datos en el _blockchain_

**Nebulas Rank** (NR) es el algoritmo de medición y valuación (_ranking_) de Nebulas, y es la unidad de medida del valor de los datos en el mundo blockchain. Sus mediciones se basan en la liquidez de los activos digitales, la interactividad entre usuarios y la propagación de activos entre ellos. NR se utiliza para clasificar y valuar direcciones, contratos inteligentes, aplicaciones distribuidas (DApps) y otras entidades en la cadena de bloques, y es de código abierto.

![Nebulas Rank](images/3.png)

#### PoD: una prueba de tu devoción hacia Nebulas

El algoritmo de consenso PoD (Proof-of-Devotion) se basa en el sistema de valuación Nebulas Rank. Mediante este mecanismo, cualquier usuario _influyente_ dentro del blockchain de Nebulas tiene la oportunidad de convertirse en un _contador_ (_bookkeeper_) y recibir recompensas por los bloques creados y las transacciones procesadas, lo cual es un aliciente para que los miembros más comprometidos de la comunidad aporten tanto a la estabilidad como a la seguridad del blockchain de Nebulas.

#### DIP: desarrolla una DApp y gana dinero en ello

Nebulas otorga recompensas a aquellos desarrolladores que contribuyen al crecimiento de su ecosistema con aplicaciones descentralizadas de calidad.

El mecanismo es sencillo: luego de que se generan una cierta cantidad de nuevos bloques en el blockchain de Nebulas, se procede a otorgar recompensas a todos aquellos desarrolladores cuyos contratos inteligentes y aplicaciones descentralizadas se implementan con un valor Nebulas Rank superior a un umbral previamente especificado. Esto permite, por un lado, recompensar por su esfuerzo a los desarrolladores que proveen valor agregado al ecosistema de Nebulas; por otro lado, incentiva la creación de nuevas DApps y contratos inteligentes de calidad.

#### Nebulas Force

Una de las mayores innovaciones que introduce Nebulas NOVA en su blockchain es la habilidad de auto-evolucionar; esta característica recibe el nombre de _Nebulas Force_ (NF). Con ella los desarrolladores son capaces de realizar cambios, incorporar nuevas tecnologías y corregir errores sin necesidad de recurrir a un _hard fork_.

Nebulas Force —y su capacidad de actualizar de forma autónoma los protocolos— se abrirá a la comunidad a medida que esta crezca. De acuerdo al peso de la _valuación NR_ de los usuarios, y utilizando el mecanismo de votación de la comunidad, ésta podrá determinar la evolución y dirección del blockchain de Nebulas y sus objetivos de actualización. Con la ayuda de la tecnología central de NF (y su apertura a la comunidad), Nebulas tendrá un potencial siempre creciente e infinitamente evolutivo.

## Capítulo 3: tutoriales para _gatos nebuleros_

Este capítulo será totalmente práctico, y explicaremos cómo hacer uso de Nebulas para sacarle el máximo provecho incluso siendo gatos. ¡Miau!

### Carteras Nebulas: instalación y uso

#### Complemento para Chrome

[![Videotutorial de la cartera Chrome de Nebulas](http://img.youtube.com/vi/X5-MLxSJEOM/0.jpg)](http://www.youtube.com/watch?v=X5-MLxSJEOM)

`Videotutorial de la cartera Chrome de Nebulas`

Tal vez la forma más sencilla de operar con la criptodivisa que nos ofrece Nebulas sea a través del complemento para Chrome. Sólo debes visitar [la página oficial](https://chrome.google.com/webstore/detail/nebulas-wallet/magbanejlegnbcppjljfhnmfmghialkl) para instalar el complemento en tu navegador Chrome.

Una vez instalado, verás un icono con el logotipo de Nebulas, cerca de la barra de direcciones. Para crear una nueva cartera, haz clic en él, y en _Get Started_.

![Vista de la carpeta](images/12.png)

Verás algo similar a esto:

![Vista de la carpeta](images/13.png)

Luego de ingresar una contraseña, verás que ya puedes operar. El complemento te indica el saldo en NAS, y abajo tienes dos botones: _Send_ and _Receive_.

![Vista de la carpeta](images/13.png)

Para conocer tu dirección NAS, pulsa en Receive.

![Vista de la carpeta](images/15.png)

Tan simple como eso. Si deseas enviar NAS a otra persona, al pulsar en _Send_ verás una pantalla similar a esta:

![Vista de la carpeta](images/16.png)

Completa la dirección (campo _To_), cantidad de NAS a enviar (campo _Amount_), y pulsa el botón _Confirm_.

![Vista de la carpeta](images/17.png)

#### Cartera web

[![Videotutorial de la cartera web de Nebulas](http://img.youtube.com/vi/-z1Hj_oGVZg/0.jpg)](http://www.youtube.com/watch?v=-z1Hj_oGVZg)

`Videotutorial de la cartera web de Nebulas`

Si no queremos saber nada con complementos para navegador, o queremos utilizar una computadora _segura_ que sólo se conecta para realizar transacciones de forma esporádica, esta es la solución ideal.

Para comenzar a utilizar esta opción, primero debes descargar la última versión desde la página oficial de Nebulas en github. Puedes obtener rápidamente la última versión desde [este enlace](https://github.com/nebulasio/web-wallet/archive/master.zip).

Descomprime el ZIP en tu escritorio, e ingresa a la carpeta generada. Verás allí varios archivos con extensión `.html`:

![Vista de la carpeta](images/2.png)

Cada uno de ellos es un módulo distinto, pero por el momento sólo te interesa uno: `index.html`. Ábrelo en tu navegador de confianza. Verás una pantalla similar a esta:

![Cartera web](images/4.png)

Primero debes ingresar una contraseña de 9 caracteres o más. **Es muy importante que recuerdes luego esa contraseña**. Una vez ingresada, pulsa donde dice _Create a New Wallet_.

![Cartera web](images/5.png)

Verás que abajo dice _Save your Keystore File_. Abajo de ese texto verás un nuevo botón, que dice _Download Keystore File_. Al pulsarlo, te dará la opción de guardar el archivo de backup de la cartera que acabas de crear.

![Cartera web](images/7.png)

**GUARDA ESE ARCHIVO EN UN LUGAR SEGURO, Y ASEGÚRATE DE COPIARLO EN OTRO SITIO POR LAS DUDAS**. Ahora bien, es un archivo JSON, que puedes abrir con cualquier editor de texto. Dentro encontrarás algo similar a esto:

```
{"version":4,"id":"Xa633703-MIAU-41bd-b449-73b9fc662308","address":"n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke","crypto":{"ciphertext":"9065b7402f5b67993641bf1110c36b98a342165miaue0300miau15010cd6b3ed","cipherparams":{"iv":"5017b610259825089c324a01cdecbf46"},"cipher":"aes-128-ctr","kdf":"scrypt","kdfparams":{"dklen":32,"salt":"a3c7eeb24d192miau56cde8f3a7712bd1f50c1f6c302da2d25351c95a953cc5b","n":4096,"r":8,"p":1},"mac":"585cad55a50168ff6a8e7ac7df90060efb248f084541cfa86c85dd09a5fd94fe","machash":"sha3256"}}
```

Por supuesto, he cambiado algunas cosas para que nadie pueda hacer uso de este archivo, ni siquiera por accidente.

Si miras con atención, verás que una de las etiquetas dentro de este archivo JSON dice `address`, y a continuación viene un texto similar a `n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke`. Pues bien: esa es la dirección NAS de la cartera que has creado. Si tu intención es la de utilizar la cartera sólo para recibir NAS (y ahorrarlos, o como se dice ahora, _HODL_), no necesitas hacer mucho más. Puedes consultar el saldo de tu cartera utilizando el siguiente formato de URL:

`https://explorer.nebulas.io/#/address/` + la dirección de tu cartera

Así, por ejemplo, si quieres consultar el saldo del ejemplo, no tienes más que visitar esta URL:

> [https://explorer.nebulas.io/#/address/n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke](https://explorer.nebulas.io/#/address/n1cZF8Kp4o8PmSyhPvmkn4VEp3phUXP1Qke)

Si deseas recibir NAS desde tu cuenta, no tienes más que compartir tu dirección con la persona que te los enviará.

Si deseas enviar NAS desde tu cuenta, debes ir a la pestaña que dice `Send`. Allí verás una pantalla similar a esta:

![Cartera web](images/8.png)

Haz clic donde dice _Select Wallet File_ y elige, en la lista de archivos, el que se generó cuando creaste la cartera.

![Cartera web](images/9.png)

Al abrirlo, deberás introducir la misma contraseña que especificaste al generar la cartera.

![Cartera web](images/10.png)

Ahora, en el campo _To Address_, coloca la dirección a la cual quieres enviar NAS. Revisa bien que esté escrita correctamente.

En el campo _Value / Amount to Send_ especifica cuántos NAS quieres enviar.

Cuando todo esté bien, pulsa el botón _Generate Transaction_. Verás que abajo aparece un recuadro QR, y un montón de texto con números. Por lo general no es necesario que toques ningún otro parámetro de lo que especifiqué.

![Cartera web](images/11.png)

Para confirmar la transacción, presiona el botón _Send Transaction_. Si todo ha salido bien, en la URL de tu dirección debería aparecer la transacción pocos segundos después de realizada.

#### Carteras de papel

Este método es un poco rebuscado, pero nos ofrece uno de los mayores niveles de seguridad, ya que crearemos una cartera _de papel_, que sirve como las viejas _libretas de ahorro_ ya que nos permite depositar NAS en ellas; si alguna vez necesitamos acceder a los NAS que almacena, podemos hacerlo mediante la cartera web (o cualquier cartera que permita importar claves privadas).

Es muy pero muy importante que esta cartera de papel se almacene en un lugar seco, a salvo de miradas indiscretas, de ladrones, etcétera. Si vas a almacenar muchos NAS, tal vez te convenga plastificar el papel, de modo que no exista chance de que la tinta se borronee. **RECUERDA QUE SI PIERDES ESTA CARTERA, PIERDES LOS NAS QUE ALLÍ ALMACENAS**. Lo bueno es que puedes hacer varias copias, por si las dudas.

Todo lo que necesitas es un trozo de papel, un bolígrafo, buena caligrafía y una copia de la cartera web. También puedes hacer uso de una impresora si lo deseas.

Sigue los pasos que detallamos más arriba para crear una dirección NAS. Anota **con letra muy clara** la dirección creada, y procede a exportar la clave privada de esa cartera. **Es vital que copies esta clave privada sin ningún tipo de errores**. Si lo deseas, puedes corroborar que la clave privada que has escrito es correcta, haciendo la prueba de importarla desde la cartera web. Envía antes 1 NAS a la dirección asociada, de modo que sepas, inequívocamente, que tienes acceso con esa clave privada.

Una vez que tengas en el papel la clave privada y la dirección NAS asociada, y que estés 100% seguro de que la clave privada está correctamente escrita, protege el papel con cinta scotch, para evitar que la tinta se corra. Si lo deseas, puedes hacer varias copias y guardarlas en lugares seguros de tu casa.

Ahora escribe la dirección NAS (que es una dirección pública) en otro papel, y guárdalo en tu bolsillo, o en cualquier otro lado en donde te resulte cómodo consultarlo con asiduidad. ¡Listo! Ya puedes comenzar a enviar NAS a esa dirección.